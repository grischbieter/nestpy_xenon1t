import nestpy


def nestFullChain( energy, xPos, yPos, zPos, interactionType ):
    detector = nestpy.XENON1T()
    
    nc = nestpy.NESTcalc(detector)

    if interactionType == "NR":
      interaction = nestpy.INTERACTION_TYPE(0)  # NR
    else:
      interaction = nestpy.INTERACTION_TYPE(8) #beta ER

    E = energy  # keV

    field = detector.FitEF(xPos, yPos, zPos)

    # Get particle yields
    #override defualt field
    y = nc.GetYields(interaction, E, drift_field = field)
    
    
    #print( "For energy %.1f keV and field %.1f V/cm\n%.1f mean photon yield and %.1f mean electron yield" % (E, y.ElectricField, y.PhotonYield, y.ElectronYield))
    
    
    q = nc.GetQuanta(y)
    
    #print( "Produced photons and electrons (recombination and statistical fluctuations) : %i photons and %i electrons" % (q.photons, q.electrons) )

    density = 2.87
    driftVelocity = nc.SetDriftVelocity(detector.get_T_Kelvin(), density, detector.FitEF(xPos, yPos, zPos) )
    central_driftV = driftVelocity #assume uniform drift speed
    S1 = nc.pyGetS1(q, xPos, yPos, zPos, driftVelocity, central_driftV, interaction, detector.FitEF(xPos, yPos, zPos), energy)
    
    driftTime = (detector.get_TopDrift() - zPos) / driftVelocity
    
    g2Info = nc.CalculateG2(True)
    
    S2 = nc.pyGetS2( q.electrons, xPos, yPos, zPos, driftTime, driftVelocity, detector.FitEF(xPos, yPos, zPos), g2Info )
    print( "rawS1, S1c, number_ext_electrons, rawS2, S2c ")
    print( S1[4], S1[5], S2[0], S2[6], S2[7] )

    return y.PhotonYield, y.ElectronYield, q.photons, q.electrons, S1[4], S1[5], S2[0], S2[6], S2[7]
    #these are mean photons, mean electrons, fluctuated photons produced, fluctuated electrons produced, unc. S1, S1c, number_ext_electrons, unc S2, S2c



output = nestFullChain(10, 0., 0., 50., "ER")
print("Everything works!")
