#!/bin/bash

#get the default NESTv2.0.1 nestpy code
git clone git@github.com:NESTCollaboration/nestpy.git



#move the edited files, and the LZ detector to the right places
cp NEST.hh nestpy/src/nestpy/.
cp NEST.cpp nestpy/src/nestpy/.
cp bindings.cpp nestpy/src/nestpy/.
cp XENON1T.hh nestpy/src/nestpy/.


#install nestpy! requires root access!!!

cd nestpy
python3 setup.py install --user

#go back to working directory
cd ..

