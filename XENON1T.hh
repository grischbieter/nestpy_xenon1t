//
// XENON1T.hh
//

#ifndef XENON1T_hh
#define XENON1T_hh 1

#include "VDetector.hh"

using namespace std;
/*********************************************************************************
 *
 *   MOST parameters in this detector file come from one of the following citations:
 *
 *   [1] "The XENON1T Dark Matter Experiment", arXiv:1708.07051. (OR,
 *       https://arxiv.org/pdf/1311.1088.pdf)
 *   [2] "Dark Matter Search Results from a One Tonne x Year Exposure of XENON1T"
 *       arXiv:1805.12562
 *   [3] "Search for New Physics with Electronic-Recoil Events in XENON1T"
 *        presentation by E. Shockley: https://uchicago.hosted.panopto.com/Panopto/Pages/Embed.aspx?id=ec125e88-cb5d-4154-bacf-abdd00d2f7c1
 *   [4]  "Energy resolution and linearity in the keV to MeV range measured in XENON1T"
 *       arXiv:2003.03825
 *
 * *******************************************************************************/
class XENON1T : public VDetector {
 public:
  XENON1T() {

    // Call the initialisation of all the parameters
    Initialization();
  };
  virtual ~XENON1T(){};

  // Do here the initialization of all the parameters that are not varying as a
  // function of time
  virtual void Initialization() {
    // Primary Scintillation (S1) parameters
    g1 = 0.1426/1.1746/0.93;  // phd per S1 phot at dtCntr (not phe). Divide out 2-PE effect
    //Taken from [3] and converted to units of phd/photon //NOTE that [4] says g1 ranges from 0.148-0.155 pe/photon (varies linearly with depth) 
    sPEres = 0.40; //single phe resolution (Gaussian assumed). pp.102-103 of https://www.physik.uzh.ch/groups/groupbaudis/darkmatter/theses/xenon/thesis_behrens.pdf
    sPEthr = 0.00;   // POD threshold in phe, usually used IN PLACE of sPEeff
    //0.35 based on LZ, as they used similar PMTs, but redundant with below
    sPEeff = 0.93;   // actual efficiency, can be used in lieu of POD threshold
    //"..resulting in an SPE acceptance of 93% with a standard deviation of 3% across all active PMTs" [2] pg 2, right column
    noiseB[0] = 0.0;  // baseline noise mean and width in PE (Gaussian)
    noiseB[1] = 0.0;  // baseline noise mean and width in PE (Gaussian)
    noiseB[2] = 0.; noiseB[3] = 0.;
    //leave all noise as zero for now
    P_dphe = 0.1746; // may actually be 0.152 acc. to [1] but close enough. Small diff
    
    coinWind = 50.; //S1 coincidence window in ns. sec 3.2 of https://arxiv.org/pdf/1708.07051.pdf
    coinLevel = 3;   // how many PMTs have to fire for an S1 to count. Same source: coincidence info from [2], pg 3, left column
    numPMTs = 127+121-36; //excluded 36 PMTs in SR1 results //For coincidence calculation //From [2] pg 2, left column
    
    extraPhot=false; //true activates EXO's W value effectively
    noiseL[0] = 0.006; //they had minimum noise according to [4]'s results
    noiseL[1] = 0.006; //they had minimum noise according to [4]'s results
    
    // Ionization and Secondary Scintillation (S2) parameters
    g1_gas = 0.101;  // phd per S2 photon in gas, used to get SE size
    //[3] states g2 = 11.55 pe/e- for the bottom PMTs; [2] pg 3 states ~63% of S2 light is collected in top array
    //infer that 37% of light is in the bottom. 11.55/0.37 = 31.216 pe/e- for the whole detector
    //Convert to phd/e-:           31.216/1.1746 = 26.576 phd/e- = g2 (if e-EE ~100%)
    //The above g1_gas value is very comparable to expectation from other TPCs, and provides SE ~26.5 phd/e-
    
    s2Fano = 1.000; //Fano-like fudge factor for SE width. Unknown so just assume Poisson. 0-2 possible, but little effect
    s2_thr = -500.; //the S2 threshold in phe or PE, *not* phd. section III A of https://arxiv.org/pdf/2006.09721.pdf
    //negative s2_thr indicates to NEST.cpp to use S2-bottom; ignores top array contribution ( see FitTBA function below )
    E_gas = 10.821; //they quote >~10kV/cm in [2] pg 2, LZ quotes 10.8 in TDR, similar size anodes/gates
    eLife_us = 650.;//measured at the end of SR1 [2] pg 2 right col. //They also state it was as low as 380us at the beginning of SR0
    
    // Thermodynamic Properties
    inGas = false;
    T_Kelvin = 177.15; // for liquid drift speed calculation
    p_bar = 1.94;      // gas pressure in units of bars, it controls S2 size
    // bottom of pg 2 left column of [2]
    
    // Data Analysis Parameters and Geometry
    dtCntr = 400.; // center of detector for S1 corrections, in usec.
    dt_min = 70.;  // minimum. Top of detector volume; yes fidu cut
    dt_max = 740.; // they quote ~700us in [2];  // maximum. Bottom of detector fiducial volume
    
    radius = 369.4;  // millimeters (fiducial rad)
    radmax = 960.0;  // actual physical geo. limit. See https://arxiv.org/pdf/1705.06655.pdf
    //rad from [1],[2]. Fid mass = 1042 +/- 12 kg
    
    // all geometry comes from [1]! Scaled so that Z=0 corresponds to bottom PMT array
    TopDrift = 2.5+1029.; //2.5 mm above gate (0.0 in their system) // mm not cm or us (but, this *is* where dt=0)
    // ADD IN DISTANCE TO bPMTs SO bPMTs ARE AT ZERO!
    // a z-axis value of 0 means the bottom of the detector (cathode OR bottom PMTs)
    // In 2-phase, TopDrift=liquid/gas border. In gas detector it's GATE, not anode!
    anode = 5.+1029.;  // the level of the anode grid-wire plane in mm
    // In a gas TPC, this is not TopDrift (top of drift region), but a few mm above it
    gate = 0.+1029.;  // mm. This is where the E-field changes (higher)
    // in gas detectors, the gate is still the gate, but it's where S2 starts
    cathode = -969.+1029.;  // mm. Defines point below which events are gamma-X

    // 2-D (X & Y) Position Reconstruction
    PosResExp = 0.015;     // exp increase in pos recon res at hi r, 1/mm
    PosResBase = 70.8364;  // baseline unc in mm, see NEST.cpp for usage
  }

  // S1 PDE custom fit for function of z
  // s1polA + s1polB*z[mm] + s1polC*z^2+... (QE included, for binom dist) e.g.
  virtual double FitS1(double xPos_mm, double yPos_mm, double zPos_mm, LCE map) {
		double topZ = 2.5+1029.;  //liquid level
		double botZ = -969.+1029.; //cathode
		double LCEmin = 0.148; //from [4]
		double LCEmax = 0.155; //from [4]
		//Ref. [4] says g1 varies linearly with Z-pos
		//LCEmax should be at the bottom (cathode)
		//get eqn of a line:
		double s1 = -7.20535e-6*zPos_mm + 0.15543;
		double s1_center = -7.20535e-6*545.75 + 0.15543;
		return s1/s1_center; //larger near the bottom! 
  }

  // Drift electric field as function of Z in mm
  // For example, use a high-order poly spline
  virtual double FitEF(double xPos_mm, double yPos_mm,
                       double zPos_mm) {  // in V/cm
		return 81.; // SR1 
		// 121 V/cm for SR0 calibrations 
  }

  // S2 PDE custom fit for function of r
  // s2polA + s2polB*r[mm] + s2polC*r^2+... (QE included, for binom dist) e.g.
  virtual double FitS2(double xPos_mm, double yPos_mm, LCE map) {
    return 1.; //no radial corrections for S2-bottom needed really
  }

  virtual vector<double> FitTBA(double xPos_mm, double yPos_mm,
                                double zPos_mm) {
    vector<double> BotTotRat(2);

    BotTotRat[0] = 0.60;  //S1 bottom-to-total ratio //assume similar ~LUX/LZ
    BotTotRat[1] = 0.37;  //from Ref [2] 
    //S2 bottom-to-total ratio, typically only used for position recon (1-this)

    return BotTotRat;
  }

  virtual double OptTrans(double xPos_mm, double yPos_mm, double zPos_mm) {
    double phoTravT, approxCenter = (TopDrift + cathode) / 2.,
                     relativeZ = zPos_mm - approxCenter;

    double A = 0.048467 - 7.6386e-6 * relativeZ +
               1.2016e-6 * pow(relativeZ, 2.) - 6.0833e-9 * pow(relativeZ, 3.);
    if (A < 0.) A = 0.;  // cannot have negative probability
    double B_a = 0.99373 + 0.0010309 * relativeZ -
                 2.5788e-6 * pow(relativeZ, 2.) -
                 1.2000e-8 * pow(relativeZ, 3.);
    double B_b = 1. - B_a;
    double tau_a = 11.15;  // all times in nanoseconds
    double tau_b = 4.5093 + 0.03437 * relativeZ -
                   0.00018406 * pow(relativeZ, 2.) -
                   1.6383e-6 * pow(relativeZ, 3.);
    if (tau_b < 0.) tau_b = 0.;  // cannot have negative time
    
    //A = 0.0574; B_a = 1.062; tau_a = 11.1; tau_b = 2.70; B_b = 1.0 - B_a;
    //LUX D-D conditions
    
    if (RandomGen::rndm()->rand_uniform() < A)
      phoTravT = 0.;  // direct travel time to PMTs (low)
    else {            // using P0(t) =
            // A*delta(t)+(1-A)*[(B_a/tau_a)e^(-t/tau_a)+(B_b/tau_b)e^(-t/tau_b)]
            // LUX PSD paper, but should apply to all detectors w/ diff #'s
      if (RandomGen::rndm()->rand_uniform() < B_a)
        phoTravT = -tau_a * log(RandomGen::rndm()->rand_uniform());
      else
        phoTravT = -tau_b * log(RandomGen::rndm()->rand_uniform());
    }

    double sig = RandomGen::rndm()->rand_gauss(
        3.84, .09);  // includes stat unc but not syst
    phoTravT += RandomGen::rndm()->rand_gauss(
        0.00, sig);  // the overall width added to photon time spectra by the
                     // effects in the electronics and the data reduction
                     // pipeline

    if (phoTravT > DBL_MAX) phoTravT = tau_a;
    if (phoTravT < -DBL_MAX) phoTravT = 0.000;

    return phoTravT;  // this function follows LUX (arXiv:1802.06162) not Xe10
                      // technically but tried to make general
  }

  virtual vector<double> SinglePEWaveForm(double area, double t0) {
    vector<double> PEperBin;

    double threshold = PULSEHEIGHT;  // photo-electrons
    double sigma = PULSE_WIDTH;      // ns
    area *= 10. * (1. + threshold);
    double amplitude = area / (sigma * sqrt(2. * M_PI)),
           signal;  // assumes perfect Gaussian

    double tStep1 = SAMPLE_SIZE / 1e2;  // ns, make sure much smaller than
                                        // sample size; used to generate MC-true
                                        // pulses essentially
    double tStep2 =
        SAMPLE_SIZE;  // ns; 1 over digitization rate, 100 MHz assumed here

    double time = -5. * sigma;
    bool digitizeMe = false;
    while (true) {
      signal = amplitude * exp(-pow(time, 2.) / (2. * sigma * sigma));
      if (signal < threshold) {
        if (digitizeMe)
          break;
        else
          ;  // do nothing - goes down to advancing time block
      } else {
        if (digitizeMe)
          PEperBin.push_back(signal);
        else {
          if (RandomGen::rndm()->rand_uniform() < 2. * (tStep1 / tStep2)) {
            PEperBin.push_back(time + t0);
            PEperBin.push_back(signal);
            digitizeMe = true;
          } else {
          }
        }
      }
      if (digitizeMe)
        time += tStep2;
      else
        time += tStep1;
      if (time > 5. * sigma) break;
    }

    return PEperBin;
  }
  // Vary VDetector parameters through custom functions
  virtual void ExampleFunction() { set_g1(0.126); }
};

#endif
